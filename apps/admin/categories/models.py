from django.db import models
from django_extensions.db.models import TimeStampedModel


class Category(TimeStampedModel):
    name = models.CharField(max_length=200, unique=True, db_index=True)

    def __unicode__(self):
        return '%s' % self.name


class NameProgram(TimeStampedModel):
    category = models.ForeignKey(Category, related_name='name_program', on_delete=models.CASCADE)
    name = models.CharField(max_length=150, unique=True, db_index=True)

    def __unicode__(self):
        return '%s' % self.name


class TitleEpisode(TimeStampedModel):
    name_program = models.ForeignKey(NameProgram, related_name='title_episode')
    name = models.CharField(max_length=150, unique=True, db_index=True)

    def __unicode__(self):
        return '%s' % self.name
