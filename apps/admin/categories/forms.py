from django import forms

from .models import Category, TitleEpisode, NameProgram


class CategoryForm(forms.ModelForm):
    class Meta:
        fields = '__all__'
        model = Category


class NameProgramForm(forms.ModelForm):
    category = forms.ModelChoiceField(queryset=Category.objects.all())

    class Meta:
        fields = '__all__'
        model = NameProgram


class TitleEpisodeForm(forms.ModelForm):
    name_program = forms.ModelChoiceField(queryset=NameProgram.objects.all())

    class Meta:
        fields = '__all__'
        model = TitleEpisode
