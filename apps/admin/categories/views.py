from django.views.generic import CreateView, DeleteView, UpdateView
from django.core.urlresolvers import reverse_lazy
from .forms import CategoryForm, TitleEpisodeForm, NameProgramForm
from .models import Category, TitleEpisode, NameProgram


class CreateCategory(CreateView):
    form_class = CategoryForm
    template_name = 'categories/add.html'


class CategoryDelete(DeleteView):
    model = Category
    template_name = 'delete/confrim_delete.html'
    success_url = reverse_lazy('category_list')


class CategoryUpdate(UpdateView):
    model = Category
    form_class = CategoryForm
    template_name = 'categories/edit.html'
    success_url = reverse_lazy('category_list')


class CreateNameProgram(CreateView):
    model = NameProgram
    form_class = NameProgramForm
    template_name = 'categories/name_programs/add.html'


class NameProgramDelete(DeleteView):
    model = NameProgram
    template_name = 'delete/confrim_delete.html'
    success_url = reverse_lazy('name_program_list')


class CreateTitleEpisode(CreateView):
    form_class = TitleEpisodeForm
    template_name = 'categories/title_episodes/add.html'


class TitleEpisodeDelete(DeleteView):
    model = TitleEpisode
    template_name = 'delete/confrim_delete.html'
    success_url = reverse_lazy('title_episode_list')
