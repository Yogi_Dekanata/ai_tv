"""video_streaming URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url
from django.views.generic import ListView

from apps.admin.categories import views
from .models import Category, NameProgram, TitleEpisode

urlpatterns = [
    url(r'^add$', views.CreateCategory.as_view(success_url='/categories/list'),
        name="add_category"),
    url(r'^list$', ListView.as_view(model=Category, template_name="categories/list.html"),
        name="category_list"),

    url(r'^update/(?P<pk>\d+)$', views.CategoryUpdate.as_view(),
        name="category_update"),

    url(r'^delete/(?P<pk>\d+)$', views.CategoryDelete.as_view(), name="category_delete"),

    url(r'^name-perogram/add$', views.CreateNameProgram.as_view(success_url='/categories/name-perogram/list'),
        name="add_name_perogram"),

    url(r'^name-perogram/list$',
        ListView.as_view(model=NameProgram, template_name="categories/name_programs/list.html"),
        name="name_perogram_list"),
    url(r'^name-perogram/delete/(?P<pk>\d+)$', views.NameProgramDelete.as_view(), name="name_program_delete"),

    url(r'^title-episodes/add$', views.CreateTitleEpisode.as_view(success_url='/categories/title-episodes/list'),
        name="add_title_episode"),
    url(r'^title-episodes/list$',
        ListView.as_view(model=TitleEpisode, template_name="categories/title_episodes/list.html"),
        name="title_episode_list"),
    url(r'^title-episodes/delete/(?P<pk>\d+)$', views.TitleEpisodeDelete.as_view(), name="title_episode_delete"),

]
