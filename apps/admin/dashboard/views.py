from django.views.generic import TemplateView
from braces.views import LoginRequiredMixin


class Dasboard(LoginRequiredMixin, TemplateView):
    template_name = 'dasboards/home.html'
