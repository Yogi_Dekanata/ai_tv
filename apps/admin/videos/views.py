from django.core.urlresolvers import reverse_lazy
from django.views.generic import CreateView, DeleteView
from braces.views import LoginRequiredMixin
from .forms import CreateVideoForm
from .models import Video


class CreateVideo(LoginRequiredMixin, CreateView):
    form_class = CreateVideoForm
    template_name = 'videos/add.html'
    success_url = reverse_lazy('video_list')
    model = Video


class VideoDelete(DeleteView):
    template_name = 'delete/confrim_delete.html'
    success_url = reverse_lazy('video_list')
    model = Video