# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('videos', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='video',
            name='category',
            field=models.ForeignKey(related_name='categories', to='categories.Category'),
        ),
        migrations.AlterField(
            model_name='video',
            name='name_program',
            field=models.ForeignKey(related_name='name_programs', to='categories.NameProgram'),
        ),
        migrations.AlterField(
            model_name='video',
            name='title_episode',
            field=models.ForeignKey(related_name='title_episodes', to='categories.TitleEpisode'),
        ),
    ]
