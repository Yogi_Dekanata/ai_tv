from django.db import models
from django_extensions.db.models import TimeStampedModel
from apps.admin.categories.models import Category, TitleEpisode, NameProgram

Chosen_rating = (('1', '1',),
                 ('2', '2',),
                 ('3', '3',),
                 ('4', '4',),
                 ('5', '5'))


class Video(TimeStampedModel):
    name = models.CharField(max_length=150, db_index=True)

    category = models.ForeignKey(Category, related_name='categories')
    name_program = models.ForeignKey(NameProgram, related_name='name_programs')
    title_episode = models.ForeignKey(TitleEpisode, related_name='title_episodes')

    source_video = models.URLField()
    rating = models.CharField(max_length=2, choices=Chosen_rating)

    def __unicode__(self):
        return '%s' % self.name
