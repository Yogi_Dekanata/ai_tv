from django import forms

from  apps.admin.categories.models import NameProgram, TitleEpisode, Category
from .models import Video


class CreateVideoForm(forms.ModelForm):
    category = forms.ModelChoiceField(queryset=Category.objects.all(), to_field_name='name')
    name_program = forms.ModelChoiceField(queryset=NameProgram.objects.all(), empty_label=None)
    title_episode = forms.ModelChoiceField(queryset=TitleEpisode.objects.all(), empty_label=None)

    class Meta:
        fields = '__all__'
        model = Video
