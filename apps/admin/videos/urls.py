from django.conf.urls import url
from django.views.generic import ListView

from apps.admin.videos import views
from apps.admin.videos.models import Video

urlpatterns = [
    url(r'^add$', views.CreateVideo.as_view(),
        name="add_video"),
    url(r'^list$', ListView.as_view(model=Video, template_name="videos/list.html"),
        name="video_list"),
    url(r'^delete/(?P<pk>\d+)', views.VideoDelete.as_view(), name='video_delete'),

]
