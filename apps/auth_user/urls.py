from django.conf.urls import url
from django.views.generic import ListView

from apps.auth_user import views

urlpatterns = [
    url(r'^login$', views.Login.as_view(),
        name="login"),
    url(r'^logout', views.Logout.as_view(),
        name="logout"),
    url(r'^registration/', views.RegistrationCustomerView.as_view(),
        name='registration'),

]
