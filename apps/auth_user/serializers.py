from django.contrib.auth import get_user_model  # If used custom user model
from rest_framework import serializers

from apps.auth_user.models import Customer

UserModel = get_user_model()


class CustomerSerializer(serializers.ModelSerializer):
    password = serializers.CharField(source='user.password')
    email = serializers.CharField(source='user.email')
    username = serializers.CharField(source='user.username')

    class Meta:
        model = Customer
        fields = ('username', 'email', 'password',)

    def create(self, validated_data):
        print validated_data
        user_data = validated_data.pop('user')
        print user_data
        print validated_data
        uniqe_id = UserModel.objects.last().id
        user = UserModel.objects.create(
            username='%s %s' % (user_data['username'], uniqe_id)
        )
        user.set_password(user_data['password'])
        user.save()

        customer = Customer.objects.create(user=user, **validated_data)

        return customer.user
