from django.conf import settings
from django.conf.urls import include, patterns, url


urlpatterns = [
    url(r'^admin/videos/', include('apps.admin.videos.urls'),name='videos'),
    url(r'^admin/categories/', include('apps.admin.categories.urls')),
    url(r'^admin/dashboard/', include('apps.admin.dashboard.urls'), name='dashboards'),
    url(r'^auth/', include('apps.auth_user.urls'), name='auth_user'),
    url(r'^select2/', include('select2.urls')),

]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )
